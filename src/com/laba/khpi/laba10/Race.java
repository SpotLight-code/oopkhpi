package com.laba.khpi.laba10;

import com.laba.khpi.laba9.List;

import java.io.Serializable;
import java.util.Date;

public class Race implements Serializable {
    private int raceNumber;
    private Date timeDeparture;
    private String[] daysForRace;
    private int freePositions;
    private List<Station> stations;

    public Race(int raceNumber, Date timeDeparture, String[] daysForRace, int freePositions, List<Station> stations) {
        this.raceNumber = raceNumber;
        this.timeDeparture = timeDeparture;
        this.daysForRace = daysForRace;
        this.freePositions = freePositions;
        this.stations = stations;
    }

    public int getRaceNumber() {
        return raceNumber;
    }

    public void setRaceNumber(int raceNumber) {
        this.raceNumber = raceNumber;
    }

    public Date getTimeDeparture() {
        return timeDeparture;
    }

    public void setTimeDeparture(Date timeDeparture) {
        this.timeDeparture = timeDeparture;
    }

    public String[] getDaysForRace() {
        return daysForRace;
    }

    public void setDaysForRace(String[] daysForRace) {
        this.daysForRace = daysForRace;
    }

    public int getFreePositions() {
        return freePositions;
    }

    public void setFreePositions(int freePositions) {
        this.freePositions = freePositions;
    }

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }


}
