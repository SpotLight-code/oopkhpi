package com.laba.khpi.laba10;

import java.util.Date;

public class Station {
    private String place;
    private Date time;


    public Station(String place, Date time) {
        this.place = place;
        this.time = time;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}

