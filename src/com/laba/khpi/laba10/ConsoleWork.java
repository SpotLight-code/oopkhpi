package com.laba.khpi.laba10;

import com.laba.khpi.laba9.List;

import java.io.IOException;


public class ConsoleWork {
    public static void main(String[] args) throws Exception {

        List<Race> bus = new List<>();

        bus.addItem(new Race(111, null, null, 0, null));
        bus.addItem(new Race(22, null, null, 0, null));
        bus.addItem(new Race(3534, null, null, 0, null));
        bus.addItem(new Race(43, null, null, 0, null));
        System.out.println(bus.size());
        bus.serialize("txt.txt");

        if (args.length == 1 && args[0].equals("auto")) {

            var Races = new List<Race>() {
                public void sort() {
                    Race[] busStat = this.getArray();
                    for (int i = 0; i < busStat.length; i++) {
                        for (int j = i; j < busStat.length - 1; j++) {
                            if (busStat[j].getRaceNumber() > busStat[j + 1].getRaceNumber()) {
                                var temp = busStat[j];
                                busStat[j] = busStat[j + 1];
                                busStat[j + 1] = temp;
                            }
                        }
                    }
                    this.clear();
                    for (Race b : busStat) {
                        this.addItem(b);
                    }
                }
            };

            Races.deserialize("txt.txt");
            Races.sort();
            Races.serialize("txt.txt");
            for (Race b : Races) {
                System.out.println(b.getRaceNumber());
            }

        }

    }
}
