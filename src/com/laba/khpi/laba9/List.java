package com.laba.khpi.laba9;

import java.io.*;
import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.Scanner;

public class List<T> implements Iterable<T>, Serializable {


    private Node<T> tempHead;
    private Node<T> head;


    @Override
    public Iterator<T> iterator() {
        if (tempHead != null) {
            tempHead = head;
        }
        return new MyIterator();
    }


    class MyIterator implements Iterator<T> {
        @Override
        public boolean hasNext() {
            if (tempHead == null) {
                return false;
            }
            return tempHead.hasNext();
        }

        @Override
        public T next() {
            tempHead = tempHead.next;
            return tempHead.element;
        }
    }


    public void addItem(T elem) {
        if (isEmpty()) {
            tempHead = new Node<>(elem);
            head = tempHead;

        }

        while (tempHead.hasNext()) {
            tempHead = tempHead.next;
        }
        tempHead.next = new Node<>(elem);
        tempHead.next.next = null;
        tempHead.next.prev = tempHead;

    }

    public boolean removeItem(T t) {
        boolean flag = false;
        while (tempHead.hasPrev()) {
            tempHead = tempHead.prev;
        }

        while (tempHead.hasNext()) {
            if (tempHead.element.equals(t)) {
                if (!tempHead.hasPrev()) {
                    tempHead = tempHead.next;
                    tempHead.prev = null;
                    head = tempHead;
                } else if (!tempHead.hasNext()) {
                    tempHead = tempHead.prev;
                    tempHead.next = null;

                } else {
                    tempHead = tempHead.prev;
                    tempHead.next = tempHead.next.next;
                    tempHead.next.prev = tempHead;
                }
                flag = true;
                break;
            }

            tempHead = tempHead.next;
        }
        return flag;
    }

    public void clear() {
        tempHead = null;
    }

    public boolean isEmpty() {
        return tempHead == null;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (T t : this) {
            stringBuilder.append(t.toString()).append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }

    public T getElement() {
        return tempHead.getElement();
    }


    public T[] getArray() {

        T[] toR = (T[]) Array.newInstance(this.getElement().getClass(), this.size());
        int i = 0;
        for (T t : this) {
            toR[i++] = t;
        }
        return toR;
    }


    public void serialize(String path) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path));
        for (T t : this) {
            oos.writeObject(t);
        }
        oos.writeObject(null);
    }

    public void deserialize(String path) throws IOException, ClassNotFoundException {
        clear();
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path));
        T t = (T) ois.readObject();
        while (t != null) {
            this.addItem(t);
            t = (T) ois.readObject();

        }
    }

    public void saveToFile(String path) throws IOException {

        FileWriter fileWriter = new FileWriter(path);
        for (T t : this) {
            fileWriter.write(t.toString() + System.lineSeparator());
        }
        fileWriter.close();

    }

    public void loadFromFile(String path) throws FileNotFoundException {
        clear();
        Scanner scanner = new Scanner(new File(path));
        while (scanner.hasNextLine()) {
            this.addItem((T) scanner.nextLine());
        }
        scanner.close();
    }

    public int size() {
        int count = 0;
        for (T ignored : this) {
            count++;
        }

        return count;
    }
}
