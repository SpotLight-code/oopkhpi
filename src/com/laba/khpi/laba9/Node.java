package com.laba.khpi.laba9;

import java.io.Serializable;

public class Node<T> implements Serializable {
    T element;
    Node<T> next;
    Node<T> prev;

    Node(T e) {
        element = e;
    }


    public T getElement() {
        return element;
    }

    private void setElement(T element) {
        this.element = element;
    }

    private Node<T> getNext() {
        return next;
    }

    private void setNext(Node<T> next) {
        this.next = next;
    }

    private Node<T> getPrev() {
        return prev;
    }

    private void setPrev(Node<T> prev) {
        this.prev = prev;
    }

    public boolean hasNext() {
        return next != null;
    }

    public boolean hasPrev() {
        return prev != null;
    }
}


