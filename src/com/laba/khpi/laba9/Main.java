package com.laba.khpi.laba9;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws Exception {
        List<String> list = new List<>();

        list.addItem("1");
        list.addItem("2");
        list.addItem("3");

        list.saveToFile("txt.txt");
        System.out.println(list);
        list.removeItem("2");
        System.out.println(list);

        list.clear();

        System.out.println("start empty");
        System.out.println(list);
        System.out.println("end empty");

        list.loadFromFile("txt.txt");
        System.out.println(list);

        list.serialize("save.dat");
        list.clear();
        list.deserialize("save.dat");

        System.out.println(list);

    }

}
